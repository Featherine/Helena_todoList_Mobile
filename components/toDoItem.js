
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Feather } from '@expo/vector-icons'; 



export default function toDoItem({ item, pressHandler }){

    return (
        <View style={styles.borderItem}>
            
                <Text style={styles.item}>{item.task}</Text><TouchableOpacity  onPress={() => pressHandler(item.key)}><Feather style={styles.feather} name="delete" size={20} color="white" />
            </TouchableOpacity>
        </View>
    )

    
}

const styles = StyleSheet.create({
    borderItem : {
        flex : 1,
        flexDirection : "row",
        justifyContent : 'space-between',
        padding : 16,
        marginTop : 16,
        borderColor : '#fff',
        borderWidth : 2,
        borderRadius : 10,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        width : 320,
    },
    item : {
               
        color : "#fff",
        fontSize: 14,
        textShadowColor: '#000',
        textShadowOffset: { width: 2, height: 2 }, textShadowRadius: 1,
    },
    feather : {
        
    }
})