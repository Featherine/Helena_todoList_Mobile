
import { StyleSheet, Text, View } from 'react-native';

export default function Header(){
    return (
        <View style={styles.header}>
            <Text style={styles.title}>メモリーの本</Text>
        </View>
    )
}

const styles= StyleSheet.create({
    header : {
        flex : 1,  
        marginTop: 30,
        
    },
    title : {
        marginTop: 13,
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold',
        textShadowColor: '#000',
        textShadowOffset: { width: 2, height: 2 }, textShadowRadius: 1,
    }

})