import { useState} from 'react';
import { StyleSheet, Text, View, FlatList, ImageBackground, Alert, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Header from "./components/header";
import AddToDo from './components/addToDo';
import ToDoItem from "./components/toDoItem";

const autreImage = require("./pictures/background.jpg");

export default function App() {

  const [todos, setTodos] = useState ([
    {task : "Écrire une nouvelle fantastique", key : "1"},
    {task : "Acheter des livres sur les plantes", key : "2"},
    {task : "Lire La Maison des Feuilles", key : "3"},
    {task : "Acheter du thé", key : "4"}
  ]);

  const pressHandler = (key) => {
    setTodos((prevTodos) => {
      return prevTodos.filter(todo => todo.key != key)
    })
  }

  const submitHandler = (text) => {

    if(text.length > 3) {
        setTodos((prevTodos) => {
        return [
          {task : text, key : Math.random().toString()}, /*pas recommandé en vrai de fairel a clé comme ça donc se renseigner */
          ...prevTodos
        ]
      })
    }
    else {
      Alert.alert('Oops ! ', "la tâche doit faire au moins quatre caractères.", [
        {text: 'Compris', onPress : () => console.log('alert closed')}
      ]);
    }
  }

  return (

    <TouchableWithoutFeedback onPress={() => {
      Keyboard.dismiss();
    }}>
      <ImageBackground source={autreImage} /*resizeMode='cover'*/ style={styles.image}>

        <View style={styles.container}>
          < Header />
          <View style={styles.content}>
            {/*to form */}
            <AddToDo submitHandler={submitHandler}/>

            <View style={styles.list}>

              <FlatList 
                data={todos}
                renderItem={({ item }) => (
                  <ToDoItem item={item} pressHandler ={pressHandler}/>
                )}
              />

            </View>

          </View>      
        </View>

      </ImageBackground>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    padding : 0,
    margin : 0,
    backgroundColor : "black",
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content : {
    flex : 7,
    padding: 40,
  },
  list : {
    flex : 1,
    marginTop : 20,
  },
});
